var game = new Phaser.Game(400, 400, Phaser.AUTO, '', {
    preload: preload,
    create: create,
    update: update
});


var multiPlayer = true;
var Level = 1;
var player1;
var player2;

var keyboard;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;
var text4;

var distance = 0;
var status = 'running';

function preload() {

    game.load.crossOrigin = 'anonymous';
    game.load.spritesheet('player1', 'image/player1.png', 32, 32);
    game.load.spritesheet('player2', 'image/player2.png', 32, 32);
    game.load.image('wall', 'image/wall.png');
    game.load.image('ceiling', 'image/ceiling.png');
    game.load.image('normal', 'image/normal.png');
    game.load.image('nails', 'image/nails.png');
    game.load.spritesheet('conveyorRight', 'image/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'image/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'image/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'image/fake.png', 96, 36);

}

function create() {

    keyboard = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D,
        'space': Phaser.Keyboard.SPACEBAR
    });
    window.onkeydown = function () {
        if (keyboard.space.isDown) game.paused = !game.paused;
    };
    createBounders();
    createplayer();
    createEndTextsBoard();
}

function update() {

    // bad
    if (status == 'gameOver' && keyboard.enter.isDown) restart();
    if (status != 'running') return;

    this.physics.arcade.collide(player1, platforms, effect);
    this.physics.arcade.collide(player1, [leftWall, rightWall]);
    checkTouchCeiling(player1);

    this.physics.arcade.collide(player2, platforms, effect);
    this.physics.arcade.collide(player2, [leftWall, rightWall]);
    checkTouchCeiling(player2);

    this.physics.arcade.collide(player1, player2, playerPush);

    checkGameOver();

    updateplayer();
    updatePlatforms();
    updateTextsBoard();

    createPlatforms();

}

function createBounders() {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;

function createPlatforms() {
    if (game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform() {

    var platform;
    var x = Math.random() * (400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;


    if (rand % (Level + 10) < 6) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand % (Level + 10) < 7) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand % (Level + 10) < 8) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand % (Level + 10) < 9) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand % (Level + 10) < 10) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createplayer() {

    player1 = game.add.sprite(200, 50, 'player1');
    player1.direction = 10;
    game.physics.arcade.enable(player1);
    player1.body.gravity.y = 500;
    player1.animations.add('left', [0, 1, 2, 3], 8);
    player1.animations.add('right', [9, 10, 11, 12], 8);
    player1.animations.add('flyleft', [18, 19, 20, 21], 12);
    player1.animations.add('flyright', [27, 28, 29, 30], 12);
    player1.animations.add('fly', [36, 37, 38, 39], 12);
    player1.life = 10;
    player1.unbeatableTime = 0;
    player1.touchOn = undefined;
    if (multiPlayer == true) {
        player2 = game.add.sprite(168, 50, 'player2');
        player2.direction = 10;
        game.physics.arcade.enable(player2);
        player2.body.gravity.y = 500;
        player2.animations.add('left', [0, 1, 2, 3], 8);
        player2.animations.add('right', [9, 10, 11, 12], 8);
        player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        player2.animations.add('flyright', [27, 28, 29, 30], 12);
        player2.animations.add('fly', [36, 37, 38, 39], 12);
        player2.life = 10;
        player2.unbeatableTime = 0;
        player2.touchOn = undefined;
    }
}

function createEndTextsBoard() {
    var style = {
        fill: '#ff0000',
        fontSize: '20px'
    }
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(180, 10, '', style);
    text3 = game.add.text(300, 10, '', style);
    text4 = game.add.text(140, 200, '~按下 Enter 重新開始~', style);
    text4.visible = false;
}

function updateplayer() {
    if (keyboard.left.isDown) {
        player1.body.velocity.x = -250;
    } else if (keyboard.right.isDown) {
        player1.body.velocity.x = 250;
    } else {
        player1.body.velocity.x = 0;
    }
    setplayerAnimate(player1);

    if (keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if (keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setplayerAnimate(player2);



}

function setplayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
        player.frame = 8;
    }
}

function updatePlatforms() {
    for (var i = 0; i < platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if (platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard() {
    text1.setText('life:' + player1.life);
    text2.setText('B' + distance);
}

function effect(player, platform) {
    if (platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if (platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if (platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if (platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if (platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if (platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function playerPush(player1, player2) {
    player1.body.velocity.x = (player1.body.velocity.x + player2.body.velocity.x) / 2 * 0.5;
    player1.body.velocity.x = (player1.body.velocity.x + player2.body.velocity.x) / 2 * 0.5;
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if (player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if (player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function () {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if (player.body.y < 0) {
        if (player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if (game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver() {
    if (multiPlayer == false) {
        if (player1.life <= 0 || player1.body.y > 500) {
            gameOver();
        }
    } else {
        if ((player1.life <= 0 || player1.body.y > 500) && (player2.life <= 0 || player2.body.y > 500)) {
            gameOver();
        }
    }
}

function gameOver() {
    text4.visible = true;
    platforms.forEach(function (s) {
        s.destroy()
    });
    platforms = [];
    status = 'gameOver';
}

function restart() {
    text4.visible = false;
    distance = 0;
    createplayer();
    status = 'running';
}