var score;
var menuState = {
    nameLabel: '',
    levelLabel: '',
    startLabel: '',
    playerLabel: '',
    playerNumber: '',
    upKey: '',
    downKey: '',
    leftKey: '',
    rightKey: '',
    enterKey: '',

    create: function () {

        this.getScore();
        // Add a background image 
        //game.add.image(0, 0, 'background');
        // Display the name of the game 
        nameLabel = game.add.text(game.width / 2, 80, '小朋友下樓梯', {
            font: '50px Arial',
            fill: '#ffffff'
        });
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen 
        levelLabel = game.add.text(game.width / 2, game.height / 2, 'Level ' + Level, {
            font: '25px Arial',
            fill: '#ffffff'
        });
        levelLabel.anchor.setTo(0.5, 0.5);
        playerNumber = 1;
        playerLabel = game.add.text(game.width / 2, game.height / 2 - 40, 'Player Number ' + playerNumber, {
            font: '25px Arial',
            fill: '#ffffff'
        });
        playerLabel.anchor.setTo(0.5, 0.5);
        scoreLabel = game.add.text(game.width / 2, game.height / 2 + 40, 'Newest Score ' + score, {
            font: '25px Arial',
            fill: '#ff3300'
        });
        scoreLabel.anchor.setTo(0.5, 0.5);

        // Explain how to start the game 
        startLabel = game.add.text(game.width / 2, game.height - 60, 'press the "enter" arrow key to start\n press "right/left" to select player number\n press "up/down" to select level', {
            font: '20px Arial',
            fill: '#ffffff'
        });
        startLabel.anchor.setTo(0.5, 0.5); // Create a new Phaser keyboard variable: the up arrow key // When pressed, call the 'start'
        upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        upKey.onDown.add(this.levelUp, this);
        downKey.onDown.add(this.levelDown, this);

        leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        leftKey.onDown.add(this.changePlayer, this);
        rightKey.onDown.add(this.changePlayer, this);

        enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);


    },
    update: function () {
        levelLabel.setText('Level ' + Level);
        playerLabel.setText('Player Number ' + playerNumber);
        //scoreLabel.setText('Newest Score ' + score);
    },

    levelUp: function () {
        if (Level < 11) Level += 1
    },
    levelDown: function () {
        if (Level > 1) Level -= 1
    },
    changePlayer: function () {
        if (multiPlayer == false) {
            multiPlayer = true;
            playerNumber = 2;
        } else {
            multiPlayer = false;
            playerNumber = 1;
        }
    },
    start: function () { // Start the actual game 
        game.state.start('play');
    },
    getScore: function () {

        //var db = firebase.firestore();
        var postsRef = firebase.database().ref('score_list');
        var total_post = [];

        var key = 0;
        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    //
                    scoreLabel.setText('Newest Score ' + childData.Score);
                    if (childData.Score > score) {

                        score = childData.Score;
                    }
                });

            })
            .catch(e => console.log(e.message));
    }
};