var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width / 2, 150, 'loading...', {
            font: '30px Arial',
            fill: '#ffffff'
        });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar var 
        progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets 
        game.load.spritesheet('player1', 'image/player1.png', 32, 32);
        game.load.spritesheet('player2', 'image/player2.png', 32, 32);
        game.load.image('wall', 'image/wall.png');
        game.load.image('ceiling', 'image/ceiling.png');
        game.load.image('basic', 'image/normal.png');
        game.load.image('nails', 'image/nails.png');
        game.load.spritesheet('rollLeft', 'image/conveyor_right.png', 96, 16);
        game.load.spritesheet('rollRight', 'image/conveyor_left.png', 96, 16);
        game.load.spritesheet('bungyJumping', 'image/trampoline.png', 96, 22);
        game.load.spritesheet('rollOver', 'image/fake.png', 96, 36);
        game.load.audio('Ahh', 'Ahh.m4a');


        // Load a new asset that we will use in the menu state 
    },
    create: function () { // Go to the menu state 
        game.state.start('login');
    }
};