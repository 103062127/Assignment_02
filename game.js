// Initialize Phaser 
var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
var Ahh;


var multiPlayer = false;
var Level = 1;
var player1;
var player2;

var keyboard;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;
var text4;
var text5;

var distance = 0;
var status = 'goingDown';

// Add all the states 
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('login', loginState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('saving', savingState);

// Start the 'boot' state 
game.state.start('boot');