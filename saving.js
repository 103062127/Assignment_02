var savingState = {
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width / 2, 150, 'loading...', {
            font: '30px Arial',
            fill: '#ffffff'
        });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar var 
        progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets 
        init(multiPlayer, distance, Level);
        // Load a new asset that we will use in the menu state 
    },
    create: function () { // Go to the menu state 
        playState.restart();
        game.state.start('play');
    }
};